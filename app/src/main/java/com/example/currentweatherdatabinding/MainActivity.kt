package com.example.currentweatherdatabinding

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.currentweatherdatabinding.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {


    lateinit var binding: ActivityMainBinding
    private lateinit var editText: EditText
    private lateinit var cities: MutableList<String>
    private lateinit var recyclerView: RecyclerView

    lateinit var fm: FragmentManager
    lateinit var ft: FragmentTransaction
    lateinit var fr1: Fragment
    lateinit var fr2: Fragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        editText = findViewById(R.id.editText)

        binding  = DataBindingUtil.setContentView(this, R.layout.activity_main)

        cities = mutableListOf("Moscow", "Irkutsk", "Samara", "Domodedovo")

        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = CustomRecyclerAdapter(cities)
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerView.scrollToPosition(Int.MAX_VALUE / 2 - Int.MAX_VALUE / 2 % cities.size - 1)

        val snapHelper = LinearSnapHelper()
        val behavior: SnapOnScrollListener.Behavior = SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL
        val onSnapPositionChangeListener: OnSnapPositionChangeListener? = null


        fm = supportFragmentManager
        ft = fm.beginTransaction()
        fr2 = DetailedWeatherFragment()
        val fr = fm.findFragmentById(R.id.frame_layout)

        if (fr == null) {
            fr1 = ShortWeatherFragment()
            fm.beginTransaction().add(R.id.frame_layout, fr1)
                .commit()
        } else
            fr1 = fr


        recyclerView.attachSnapHelperWithListener(snapHelper, behavior, onSnapPositionChangeListener, cities, this, fr1 as ShortWeatherFragment, fr2 as DetailedWeatherFragment)
    }



    fun addCity(v: View){
        val city = binding.editText.text.toString()
        binding.editText.text.clear()
        Log.d("NIKITA", "Text: $city")
        cities.add(city)
        recyclerView.adapter?.notifyDataSetChanged()
        recyclerView.scrollToPosition(Int.MAX_VALUE / 2 - Int.MAX_VALUE / 2 % cities.size - 2)
    }

    fun designClick(v: View){
        MyDialog(this, fm, fr1, fr2).show(supportFragmentManager, "test")
    }
}